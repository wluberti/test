<?php declare(strict_types=1);

require_once('../vendor/autoload.php');

echo <<<HTMLBLOB
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Boilerplate test</title>
</head>
<body>

<h1 align="center">Boilerplate test</h1>

</body>
</html>
HTMLBLOB;